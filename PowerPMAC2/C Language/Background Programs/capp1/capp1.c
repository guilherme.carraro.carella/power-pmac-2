/*For more information see notes.txt in the Documentation folder */
#include <gplib.h>   
#include <stdio.h>

#include <RtGpShm.h>

#define _PPScriptMode_		// for enum mode, replace this with #define _EnumMode_
#include "../../Include/pp_proj.h"

int main(void)
{
	InitLibrary();  // Required for accessing Power PMAC library

	int i;

	FILE *fp;

	fp=fopen("temp.txt", "w");
	
	if(fp == NULL){
		perror("Error Opening File");
		exit(-1);
	}

	for(i=0;i<=ArrayLength;i++){
		fprintf(fp,"%lf",BatchPos(i));
		
		if(i<ArrayLength){
			fprintf(fp,",");
		}

	}
	
	fclose(fp);

	CloseLibrary();

	return 0;
}

